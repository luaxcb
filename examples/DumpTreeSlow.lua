require("lxcb")

local INDENT_PER_LEVEL=2
local no_name = ""

local WM_NAME

function GetWMName(id)
    if (WM_NAME == nil) then
	local name = 'WM_NAME'
	WM_NAME = dpy:intern_atom_reply(dpy:intern_atom(false, string.len(name), name)).atom
    end
    local prop, propreq
    propreq = dpy:get_property(false, id, WM_NAME, dpy.GET_PROPERTY_TYPE.ANY, 0, 80)
    prop = dpy:get_property_reply(propreq)
    if (prop) then
	return string.char(unpack(prop.value))
    else
	return nil
    end
end

function printf(...)
    io.output():write(string.format(...))
end

function DumpTree(id, indent)
    local root, parent, children

    -- Show the information for this window
    local mapped = true
    local name

    local xwa = dpy:get_window_attributes_reply(dpy:get_window_attributes(id))
    if (xwa) then
	mapped = (xwa.map_state == dpy.MAP_STATE.VIEWABLE)
    end

    local tp = GetWMName(id) 
    name = tp or ""

    if (mapped) then
	printf("%s0x%08X ", string.rep(" ", indent), id);
    else
	printf("%s(0x%08X)", string.rep(" ", indent), id);
    end

    local geom = dpy:get_geometry_reply(dpy:get_geometry(id))
    printf(" %d: %d,%d [%dx%d]", geom.depth, geom.x, geom.y, geom.width, geom.height)
    
    printf(" %s\n", name);
    
    -- Recursively show the information for the rest of the tree
    local tree = dpy:query_tree_reply(dpy:query_tree(id))
    for k, v in ipairs(tree.children) do
	DumpTree(v, indent + INDENT_PER_LEVEL)
    end
end


local root
dpy, screen = lxcb.connect("")

if (not dpy) then
    print("Error: Cannot open default display.")
    return 1;
end

if (#arg >= 1) then
    root = tonumber(arg[1])
    if (not root) then
	print("Usage: DumpTree [starting window]")
	return 1;
    end
else
    root = dpy:get_setup().roots[screen+1].root
end

DumpTree(root, 0)

dpy = nil

return 0
