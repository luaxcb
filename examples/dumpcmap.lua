require("lxcb")

do
    options = {}
    local optlist = {
	{ shortname = 'h', longname = 'help', desc = 'Show this help menu' },
	{ longname = 'display', desc = 'Set the display to connect to', arg = true },
    }
    local shorts = {}
    local longs = {}
    
    table.sort(optlist, function(a, b) return a.longname < b.longname end)
    
    local function usage()
	print("\nUsage: dumpcmap <colormap id>\n")
	for _, o in ipairs(optlist) do
	    local str
	    if o.shortname then
		str = '-' .. o.shortname
	    else
		str = '  '
	    end
	    print(string.format('   %s --%-15s %s', str, o.longname, o.desc))
	end
	print('')
	os.exit(1)
    end

    for _, i in ipairs(optlist) do
	if i.shortname then
	    shorts[i.shortname] = i.longname
	end
	if i.longname then
	    longs[i.longname] = i
	end
    end
    
    local nextname
    
    for _, i in ipairs(arg) do
	if nextname then
	    options[nextname] = i
	    nextname = nil
	else
	    if string.sub(i, 1, 1) == '-' then
		if string.sub(i, 2, 2) == '-' then
		    local opt = string.sub(i,3)
		    local val = longs[opt]
		    if val then
			if val.arg then
			    nextname = opt
			else
			    options[opt] = true
			end
		    else
			print("Unknown long option "..i)
			usage()
		    end
		else
		    for o in string.gmatch(string.sub(i, 2), '.') do
			if shorts[o] then
			    options[shorts[o]] = true
			else
			    print("Unknown option -"..o)
			    usage()
			end
		    end
		end
	    else
		if options.colormap then
		    usage()
		else
		    options.colormap = i
		end
	    end
	end
    end
    
    if not options.colormap then
	usage()
    end
    
    if nextname then
	usage()
    end
    
    if options.help then
	usage()
    end
end

local x = lxcb.connect(options.display)
if (not x) or x:has_error() then
    print("Error: cannot open display")
    os.exit(1)
end

local query = {}
for i = 1, 0x100 do
    query[i] = i - 1
end
local cookie = x:query_colors(options.colormap, query)

query = x:query_colors_reply(cookie)
if not query then
    print("Invalid colormap ID")
    os.exit(1)
end

for i = 1, query.colors_len do
    print(string.format("RGB[%03d] = %04X %04X %04X",
	i,
	query.colors[i].red,
	query.colors[i].green,
	query.colors[i].blue))
end
