require("lxcb")

local INDENT_PER_LEVEL=2
local no_name = ""

local queue = {};
local window = {};
local atom = {};

function internatom(this, name)
    this[name] = dpy:intern_atom_reply(dpy:intern_atom(false, string.len(name), name)).atom
    return this[name]
end

function printf(...)
    io.output():write(string.format(...))
end

function DumpTree(id, indent)
    -- Show the information for this window
    local win = window[id]

    if (win.mapped) then
	printf("%s0x%08X ", string.rep(" ", indent), id);
    else
	printf("%s(0x%08X)", string.rep(" ", indent), id);
    end

    local geom = win.geom
    printf(" %d: %d,%d [%dx%d]", geom.depth, geom.x, geom.y, geom.width, geom.height)
    
    printf(" %s\n", win.name or "");
    
    -- Recursively show the information for the rest of the tree
    for k, v in ipairs(win.children) do
	DumpTree(v, indent + INDENT_PER_LEVEL)
    end
end

-- Queueing functions
function QueueInternAtom(name)
    local cookie = dpy:intern_atom(false, string.len(name), name)
    table.insert(queue, function()
	atom[name] = dpy:intern_atom_reply(cookie).atom
    end)
end

function QueueWindowName(id)
    local cookie = dpy:get_property(false, id, atom.WM_NAME, dpy.GET_PROPERTY_TYPE.ANY, 0, 80)
    table.insert(queue, function()
	local prop = dpy:get_property_reply(cookie)
	if (prop) then
	    window[id].name = string.char(unpack(prop.value))
	end
    end)
end

function QueueWindowGeometry(id)
    local cookie = dpy:get_geometry(id)
    table.insert(queue, function()
	window[id].geom = dpy:get_geometry_reply(cookie)
    end)
end

function QueueWindowMapped(id)
    local cookie = dpy:get_window_attributes(id)
    table.insert(queue, function()
	local xwa = dpy:get_window_attributes_reply(cookie)
	window[id].mapped = (xwa.map_state == dpy.MAP_STATE.VIEWABLE)
    end)
end

function QueueWindowChildren(id)
    local cookie = dpy:query_tree(id)
    table.insert(queue, function()
	local tree = dpy:query_tree_reply(cookie)
	window[id].children = tree.children
	
	-- For each window, query the children first
	-- (since that's the one that'll cause recursive
	--  queries), and then query incidental per-window data
	-- (hopefully overlapping with the child queries)
	    
	for k, v in ipairs(tree.children) do
	    window[v] = {}
	    QueueWindowChildren(v)
	end
	
	for k, v in ipairs(tree.children) do
	    QueueWindowName(v)
	    QueueWindowGeometry(v)
	    QueueWindowMapped(v)
	end
    end)
end

function ProcessQueue()
    local next = 1
    while (#queue >= next) do
	queue[next]()
	next = next + 1
    end
end

-- Atom cache setup
setmetatable(atom, { __index = internatom })

-- Main
local root
dpy, screen = lxcb.connect("")

if (not dpy) then
    print("Error: Cannot open default display.")
    return 1;
end

if (#arg >= 1) then
    root = tonumber(arg[1])
    if (not root) then
	print("Usage: DumpTree [starting window]")
	return 1;
    end
else
    root = dpy:get_setup().roots[screen+1].root
end

window[root] = { mapped = true }
QueueWindowChildren(root)
QueueInternAtom('WM_NAME')
QueueWindowGeometry(root)
ProcessQueue()

DumpTree(root, 0)

dpy = nil

return 0
