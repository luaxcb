require("lxcb")

local INDENT_PER_LEVEL=2
local no_name = ""

local atom = {}

function internatom(this, name)
    this[name] = dpy:intern_atom_reply(dpy:intern_atom(false, string.len(name), name)).atom
    return this[name]
end

setmetatable(atom, { __index = internatom })

function printf(...)
    io.output():write(string.format(...))
end

function DumpTree(id, indent)
    -- Show the information for this window
    local mapped
    
    local attrcookie = dpy:get_window_attributes(id)
    local geomcookie = dpy:get_geometry(id)
    local namecookie = dpy:get_property(false, id, atom.WM_NAME, dpy.GET_PROPERTY_TYPE.ANY, 0, 80)
    local treecookie = dpy:query_tree(id)

    local xwa = dpy:get_window_attributes_reply(attrcookie)
    if (xwa) then
	mapped = (xwa.map_state == dpy.MAP_STATE.VIEWABLE)
    end

    if (mapped) then
	printf("%s0x%08X ", string.rep(" ", indent), id);
    else
	printf("%s(0x%08X)", string.rep(" ", indent), id);
    end

    local geom = dpy:get_geometry_reply(geomcookie)
    printf(" %d: %d,%d [%dx%d]", geom.depth, geom.x, geom.y, geom.width, geom.height)
    
    local name = ''
    local prop = dpy:get_property_reply(namecookie)
    if (prop) then
	name = prop.value
    end

    printf(" %s\n", name);
    
    -- Recursively show the information for the rest of the tree
    local tree = dpy:query_tree_reply(treecookie)
    for k, v in ipairs(tree.children) do
	DumpTree(v, indent + INDENT_PER_LEVEL)
    end
end


local root
dpy, screen = lxcb.connect("")

if (not dpy) then
    print("Error: Cannot open default display.")
    return 1;
end

if (#arg >= 1) then
    root = tonumber(arg[1])
    if (not root) then
	print("Usage: DumpTree [starting window]")
	return 1;
    end
else
    root = dpy:get_setup().roots[screen+1].root
end

DumpTree(root, 0)

dpy = nil

return 0
