require("lxcb")

function printf(...)
    io.output():write(string.format(...))
end

local atom = {}
local atomname = {}

function internatom(this, name)
    local id = c:intern_atom_reply(c:intern_atom(false, string.len(name), name)).atom
    this[name] = id
    atomname[id] = name
    return id
end

function getatomname(this, id)
    local name = string.char(unpack(c:get_atom_name_reply(c:get_atom_name(id)).name))
    this[id] = name
    atom[name] = id
    return name
end

setmetatable(atom, { __index = internatom })
setmetatable(atomname, { __index = getatomname })


function FontCursor(c, id)
    local font = c:generate_id()
    local cursor = c:generate_id()
    local fn = "cursor"
    c:open_font(font, string.len(fn), fn)
    c:create_glyph_cursor(cursor, font, font, id, id+1, 0xFFFF, 0xFFFF, 0xFFFF, 0, 0, 0)
    c:close_font(font)
    
    return cursor
end

 -- Routine to let user select a window using the mouse
function Select_Window(c)
    local status
    local cursor
    local event
    local target_win = 0
    local root = c:get_setup().roots[screen+1].root
    local buttons = 0
    
    -- Make the target cursor
    cursor = FontCursor(c, 34); -- XC_crosshair

    -- Grab the pointer using target cursor, letting it room all over
    local grabreq = c:grab_pointer(false, root, 
				c.EVENT_MASK.BUTTON_PRESS + c.EVENT_MASK.BUTTON_RELEASE,
				c.GRAB_MODE.SYNC, c.GRAB_MODE.ASYNC,
				root, cursor, 0) -- CurrentTime)
    status = c:grab_pointer_reply(grabreq).status
    if (status ~= c.GRAB_STATUS.SUCCESS) then
	printf("Can't grab the mouse.\n")
	return 0;
    end

    -- Let the user select a window...
    while ((target_win == 0) or (buttons ~= 0)) do
	-- allow one more event
	c:allow_events(c.ALLOW.SYNC_POINTER, 0) -- CurrentTime)
	
	c:flush()  -- wait_for_event doesn't automatically flush.
	event = c:wait_for_event()
	
	if (event.response_type == 4) -- "ButtonPress")
	then
	    if (target_win == 0) then
		target_win = event.child -- window selected
		if (target_win == 0) then target_win = event.event end
		if (target_win == 0) then target_win = event.root end
	    end
	    buttons = buttons + 1
	elseif (event.response_type == 5) -- "ButtonRelease")
	then
	    if (buttons > 0) then -- there may have been some down before we started
		buttons = buttons - 1
	    end
	end
    end

    c:ungrab_pointer(0) -- CurrentTime)      -- Done with pointer

    printf("target_win = 0x%08X\n", target_win)
    return target_win
end

function string2int(s)
    -- assume we're running on an LSB machine
    local multiplier = 1;
    local value = 0;
    local bytes = { string.byte(s, 1, #s) }
    for k,v in ipairs(bytes) do
	value = value + v * multiplier
	multiplier = multiplier * 0x100
    end
    return value
end

local dumper = {
	STRING 		= function(prop) printf(" = %s", prop) end,
	UTF8_STRING 	= function(prop) printf(" = %s", prop) end,
	CARDINAL	= function(prop) printf(" = 0x%X", string2int(prop)) end,
	ATOM		= function(prop)
		printf(" = ")
		while (#prop >= 4) do
		    local atom = string2int(string.sub(prop, 1, 4))
		    prop = string.sub(prop, 5);
		    printf("%s, ", atomname[atom])
		end
	end,
}


function DumpProps(c, id)
    local list = c:list_properties_reply(c:list_properties(id)).atoms;
    for k, v in ipairs(list) do
	local name = atomname[v]
	printf("  %s: ",name);
	local prop = c:get_property_reply(c:get_property(false, id, v, c.GET_PROPERTY_TYPE.ANY, 0, 100))
	if (prop) then
	    local typename = atomname[prop.type]
	    printf("%s", typename)
	    if (dumper[typename]) then dumper[typename](prop.value) end
	end
	printf("\n");
    end
end

local root
c, screen = lxcb.connect()

if (not c) then
    print("Error: Cannot open default display.")
    return 1;
end

if (#arg >= 1) then
    root = tonumber(arg[1])
    if (not root) then
	print("Usage: ShowProps [window]")
	return 1;
    end
else
    root = Select_Window(c)
end

DumpProps(c, root)

c = nil

return 0
