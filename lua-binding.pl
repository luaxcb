#!/usr/bin/perl

# Known bugs:
# - Does not support _checked or _unchecked variants of function calls
# - Allows Lua to underflow and (maybe) crash C, when it should lua_error instead (so pcall can catch it)
# - ChangeProperty is limited to the 8-bit datatype

# Known warts:
# - Should get string lengths (and other lengths) from Lua, instead of requiring the length to be passed from the script

use warnings;
use strict;

use XML::Simple qw(:strict);


my %xcbtype = (
    BOOL => 'uint8_t',
    BYTE => 'uint8_t',
    CARD8 => 'uint8_t',
    CARD16 => 'uint16_t',
    CARD32 => 'uint32_t',
    INT8 => 'int8_t',
    INT16 => 'int16_t',
    INT32 => 'int32_t',
    
    char => 'const char',
    void => 'const void',  # Hack, to partly support ChangeProperty, until we can reverse 'op'.
    float => 'float',
    double => 'double',
);

my %luatype = (
    BOOL => 'boolean',
    BYTE => 'integer',
    CARD8 => 'integer',
    CARD16 => 'integer',
    CARD32 => 'integer',
    INT8 => 'integer',
    INT16 => 'integer',
    INT32 => 'integer',
    
    char => 'integer',
    void => 'integer',  # Hack, to partly support ChangeProperty, until we can reverse 'op'.
    float => 'number',
    double => 'number',
);

my %luachecktype = (
    BOOL => 'LUA_TBOOLEAN',
    BYTE => 'LUA_TNUMBER',
    CARD8 => 'LUA_TNUMBER',
    CARD16 => 'LUA_TNUMBER',
    CARD32 => 'LUA_TNUMBER',
    INT8 => 'LUA_TNUMBER',
    INT16 => 'LUA_TNUMBER',
    INT32 => 'LUA_TNUMBER',
    
    char => 'LUA_TNUMBER',
    void => 'LUA_TNIL',
    float => 'LUA_TNUMBER',
    double => 'LUA_TNUMBER',
);

sub mangle($;$)
{
    my %simple = (
	CHAR2B => 1,
	INT64 => 1,
	FLOAT32 => 1,
	FLOAT64 => 1,
	BOOL32 => 1,
	STRING8 => 1,
	Family_DECnet => 1
    );
    my $name = shift;
    my $clean = shift;
    my $mangled = '';
    
    $mangled = 'xcb_' unless ($clean);
    
    if ($simple{$name}) {
	$mangled .= lc($name);
    } else {
	while (length ($name)) {
	    $name =~ /^(.)(.*)$/;
	    my $char = $1;
	    my $next = $2;
	
	    $mangled .= lc($char);
	    
	    if (
		$name =~ /^[[:lower:]][[:upper:]]/ ||
		$name =~ /^\d[[:alpha:]]/ ||
		$name =~ /^[[:alpha:]]\d/ ||
		$name =~ /^[[:upper:]][[:upper:]][[:lower:]]/)
	    {
		$mangled .= '_';
	    }
	    
	    $name = $next;
	}
    }
    return $mangled;
}

sub cname($)
{
    my %bad = (
	new => 1,
	delete => 1,
	class => 1,
	operator => 1 );
    my $name = shift;
    return "_$name" if ($bad{$name});
    return $name;
}

sub do_push($$;$)
{
    my $indent = ' ' x ((shift) * 4);
    my $type = shift;
    my $name = shift;
    
    my $base;
    
    if (defined($name)) {
	$base = "x->".cname($name);
    } else {
	$base = "i.data";
    }
    
    if ($luatype{$type}) {
	# elemental type
	$base = '*'.$base if (!defined($name));
	print OUT $indent."lua_push".$luatype{$type}."(L, $base);\n";
    } else {
	# complex type
	$base = '&'.$base if (defined($name));
	print OUT $indent."push_$type(L, $base);\n";
    }
}

sub do_push_list($$$$;$)
{
    my $indent = shift;
    my $name = shift;
    my $type = shift;
    my $fn = shift;
    my $const = shift;
    
    my $spaces = ' ' x (($indent) * 4);
    my $mtype = mangle($type);
    
    if ($type eq 'char') {
	# String
	my $get = mangle($name)."_".$fn;
	print OUT $spaces."lua_pushlstring(L, ";
	print OUT "$get(x), ";
	print OUT $get."_length(x) );\n";
	print OUT $spaces."lua_setfield(L, -2, \"$fn\");\n";
    } elsif ($type eq 'void') {
	# Evil hack for GetProperty
	my $get = mangle($name)."_".$fn;
	print OUT $spaces."lua_pushlstring(L, ";
	print OUT "$get(x), ";
	print OUT $get."_length(x) * (x->format / 8) );\n";
	print OUT $spaces."lua_setfield(L, -2, \"$fn\");\n";
    } elsif (defined($luatype{$type})) {
	# Array of elemental type
	my $get = mangle($name)."_".$fn;
	print OUT $spaces."{\n";
	print OUT "$spaces    const ".$xcbtype{$type}." *i;\n";
	print OUT "$spaces    int len, j;\n\n";
	if (defined($const)) {
	    my $cfn = cname($fn);
	    my $len = $const->[0];
	    print OUT "$spaces    i = x->$cfn;\n";
	    print OUT "$spaces    len = $len;\n";
	} else {
	    print OUT "$spaces    i = $get(x);\n";
	    print OUT "$spaces    len = $get"."_length(x);\n";
	}
	print OUT "$spaces    lua_newtable(L);\n";
	print OUT "$spaces    for (j=0; j<len; j++) {\n";
	print OUT "$spaces        lua_push".$luatype{$type}."(L, i[j]);\n";
	print OUT "$spaces        lua_rawseti(L, -2, j+1);\n";
	print OUT "$spaces    }\n";
	print OUT "$spaces    lua_setfield(L, -2, \"$fn\");\n";
	print OUT "$spaces}\n";
    } else {
	# Array of complex type
	print OUT $spaces."{\n";
	print OUT "$spaces    $mtype"."_iterator_t i;\n";
	print OUT "$spaces    int j=0;\n\n";
	print OUT "$spaces    i = ".mangle($name)."_".$fn."_iterator(x);\n";
	print OUT "$spaces    lua_newtable(L);\n";
	print OUT "$spaces    while (i.rem) {\n";
	do_push($indent + 2, $type);
	print OUT "$spaces        lua_rawseti(L, -2, ++j);\n";
	print OUT "$spaces        $mtype"."_next(&i);\n";
	print OUT "$spaces    }\n";
	print OUT "$spaces    lua_setfield(L, -2, \"$fn\");\n";
	print OUT "$spaces}\n";
    }
}

sub do_structs($)
{
    my $xcb = shift;
    foreach my $struct (@{$xcb->{'struct'}}) {
	my $name = $struct->{'name'};
	my $xcbname = mangle($name).'_t';
	my $dogetter = 1;
	
	my %nostatic = ( # These structs are used from the base protocol
	    xcb_setup_t => 1,
	);
	
	print OUT "static " unless ($nostatic{$xcbname});
	
	print OUT "void push_$name (lua_State *L, const $xcbname *x)\n";
	print OUT "{\n";
	print OUT "    lua_newtable(L);\n";
	foreach my $field (@{$struct->{'field'}}) {
	    my $fn = $field->{'name'}; 
	    do_push(1, $field->{'type'}, $fn);
	    print OUT "    lua_setfield(L, -2, \"$fn\");\n";
	}
	if ($struct->{'list'}) {
	    $dogetter = 0;  # If it has a list, the get half shouldn't (can't?) be needed.
	    foreach my $list (@{$struct->{'list'}}) {
		do_push_list(1, $name, $list->{'type'}, $list->{'name'}, $list->{'value'});
	    }
	}
	print OUT "}\n\n";

	if ($dogetter) {
	    print OUT "static void get_$name (lua_State *L, int pos, $xcbname *x)\n";
	    print OUT "{\n";
	    print OUT "    luaL_checktype(L, pos, LUA_TTABLE);\n";
	    foreach my $field (@{$struct->{'field'}}) {
		my $fn = $field->{'name'};
		my $type = $luatype{$field->{'type'}};
		my $checktype = $luachecktype{$field->{'type'}};
		my $cfn = cname($fn);
		print OUT "    lua_getfield(L, pos, \"$fn\");\n";
		print OUT "    luaL_checktype(L, -1, $checktype);\n";
		print OUT "    x->$cfn = lua_to$type(L, -1);\n";
		print OUT "    lua_pop(L, 1);\n";
	    }
	    print OUT "}\n\n";
	}
    }
    foreach my $union (@{$xcb->{'union'}}) {
	my $name = $union->{'name'};
	my $xcbname = mangle($name).'_t';
	
	print OUT "static ";
	print OUT "void push_$name (lua_State *L, const $xcbname *x)\n";
	print OUT "{\n";
	print OUT "    lua_newtable(L);\n";
	foreach my $field (@{$union->{'field'}}) {
	    my $fn = $field->{'name'}; 
	    do_push(1, $field->{'type'}, $fn);
	    print OUT "    lua_setfield(L, -2, \"$fn\");\n";
	}
	if ($union->{'list'}) {
	    foreach my $list (@{$union->{'list'}}) {
		do_push_list(1, $name, $list->{'type'}, $list->{'name'}, $list->{'value'});
	    }
	}
	print OUT "}\n\n";
    }
}

sub do_typedefs($)
{
    my $xcb = shift;
    foreach my $tdef (@{$xcb->{'typedef'}}) {
	$xcbtype{$tdef->{'newname'}} = $xcbtype{$tdef->{'oldname'}};
	$luatype{$tdef->{'newname'}} = $luatype{$tdef->{'oldname'}};
	$luachecktype{$tdef->{'newname'}} = $luachecktype{$tdef->{'oldname'}};
    }
    foreach my $tdef (@{$xcb->{'xidtype'}}) {
	$xcbtype{$tdef->{'name'}} = $xcbtype{'CARD32'};
	$luatype{$tdef->{'name'}} = $luatype{'CARD32'};
	$luachecktype{$tdef->{'name'}} = $luachecktype{'CARD32'};
    }
    foreach my $tdef (@{$xcb->{'xidunion'}}) {
	$xcbtype{$tdef->{'name'}} = $xcbtype{'CARD32'};
	$luatype{$tdef->{'name'}} = $luatype{'CARD32'};
	$luachecktype{$tdef->{'name'}} = $luachecktype{'CARD32'};
    }
}

sub get_vartype($)
{
    my $type = shift;
    return $xcbtype{$type} if (defined ($xcbtype{$type}));
    return mangle($type)."_t";
}

sub do_get($$$;$)
{
    my $index = shift;
    my $type = shift;
    my $name = shift;
    my $indent = shift;
    
    $indent = 1 unless (defined($indent));
    $indent = ' ' x (($indent) * 4);
    
    if ($luatype{$type}) {
	# elemental type
	print OUT $indent.$name." = lua_to".$luatype{$type}."(L, $index);\n";
    } else {
	# complex type
	print OUT $indent."get_$type(L, $index, &$name);\n";
    }
}

sub do_get_list($$$;$)
{
    my $index = shift;
    my $type = shift;
    my $name = shift;
    my $indent;
    my $autolen = shift;
    
    $indent = 1 unless (defined($indent));
    my $spaces = ' ' x (($indent) * 4);
    
    if ($type eq 'char' or $type eq 'void') {
	# Simple case: String
	print OUT $spaces.$name." = lua_tostring(L, $index);\n";
	return;
    }

    print OUT $spaces."{\n";
    print OUT "$spaces    size_t i, count;\n";
    print OUT "$spaces    int idx = lua_gettop(L) + 1;\n";
    print OUT "$spaces    luaL_checktype(L, $index, LUA_TTABLE);\n";
    if ($autolen) {
	print OUT "$spaces    ${name}_len = lua_objlen(L, $index);\n";
	print OUT "$spaces    count = ${name}_len;\n";
    } else {
	print OUT "$spaces    count = lua_objlen(L, $index);\n";
    }
    print OUT "$spaces    $name = malloc(count * sizeof(";
    
    if ($xcbtype{$type}) {
	# elemental type
	print OUT $xcbtype{$type};
    } else {
	# complex type
	print OUT mangle($type)."_t";
    }
    print OUT "));\n";
    print OUT "$spaces    for (i=0; i<count; i++) {\n";
    print OUT "$spaces        lua_rawgeti(L, $index, i+1);\n";
    do_get('idx', $type, $name.'[i]', $indent + 2);
    print OUT "$spaces        lua_pop(L, 1);\n";
    print OUT "$spaces    }\n";
    
    print OUT $spaces."}";
}

sub do_requests($\%)
{
    my $xcb = shift;
    my $func = shift;
    
    foreach my $req (@{$xcb->{'request'}}) {
	# Function header
	print OUT "static int ";
	print OUT $req->{'name'};
	print OUT "(lua_State *L)\n{\n";
	
	my $cookie = mangle($req->{'name'})."_cookie_t";
	
	# Declare variables
	if (defined($req->{'reply'})) {
	    print OUT "    $cookie *cookie;\n";
	}
	print OUT "    xcb_connection_t *c;\n";
	foreach my $var (@{$req->{'field'}}) {
	    print OUT "    ".get_vartype($var->{'type'})." ";
	    print OUT $var->{'name'}.";\n";
	}
	if (defined($req->{'list'})) {
	    foreach my $var (@{$req->{'list'}}) {
		if (!defined($var->{'fieldref'}) && !defined($var->{'op'}) && !defined($var->{'value'})) {
		    print OUT "    uint32_t ";
		    print OUT $var->{'name'}."_len;\n";
		}
		print OUT "    ".get_vartype($var->{'type'})." *";
		print OUT $var->{'name'}.";\n";
	    }
	}
	if (defined($req->{'valueparam'})) {
	    foreach my $var (@{$req->{'valueparam'}}) {
		print OUT "    ".get_vartype($var->{'value-mask-type'})." ";
		print OUT $var->{'value-mask-name'}.";\n";
		print OUT "    uint32_t *".$var->{'value-list-name'}.";\n";
	    }
	}
	print OUT "\n";
	
	# Set up userdata
	if (defined($req->{'reply'})) {
	    print OUT "    lua_newuserdata(L, sizeof(*cookie));\n";
	    print OUT "    luaL_getmetatable(L, \"$cookie\");\n";
	    print OUT "    lua_setmetatable(L, -2);\n";
	    print OUT "    cookie = ($cookie *)lua_touserdata(L, -1);\n";
	    print OUT "    lua_createtable(L, 0, 2);\n";
	    print OUT "    lua_pushvalue(L, 1);\n";
	    print OUT "    lua_setfield(L, -2, \"display\");\n";
	    print OUT "    lua_pushboolean(L, 1);\n";
	    print OUT "    lua_setfield(L, -2, \"collect\");\n";
	    print OUT "    lua_setfenv(L, -2);\n\n";
	}
	
	# Read variables from lua
	print OUT "    c = ((xcb_connection_t **)luaL_checkudata(L, 1, \"XCB.display\"))[0];\n";
	my $index = 1;
	foreach my $var (@{$req->{'field'}}) {
	    do_get(++$index, $var->{'type'}, $var->{'name'});
	}
	if (defined($req->{'list'})) {
	    foreach my $var (@{$req->{'list'}}) {
		if (!defined($var->{'fieldref'}) && !defined($var->{'op'}) && !defined($var->{'value'})) {
		    # do_get(++$index, 'CARD32', $var->{'name'}."_len");
		    do_get_list(++$index, $var->{'type'}, $var->{'name'}, 1);
		} else {
		    do_get_list(++$index, $var->{'type'}, $var->{'name'});
		}
	    }
	}
	if (defined($req->{'valueparam'})) {
	    foreach my $var (@{$req->{'valueparam'}}) {
		do_get(++$index, $var->{'value-mask-type'}, $var->{'value-mask-name'});
		do_get_list(++$index, 'CARD32', $var->{'value-list-name'});
	    }
	}
	print OUT "\n";
	
	# Function call
	print OUT "    ";
	if (defined($req->{'reply'})) {
	    print OUT "*cookie = ";
	}
	print OUT mangle($req->{'name'})."(";
	my $glob = 'c, ';
	foreach my $var (@{$req->{'field'}}) {
	    $glob .= $var->{'name'};
	    $glob .= ", ";
	}
	if (defined($req->{'list'})) {
	    foreach my $var (@{$req->{'list'}}) {
		if (!defined($var->{'fieldref'}) && !defined($var->{'op'}) && !defined($var->{'value'})) {
		    $glob .= $var->{'name'}.'_len';
		    $glob .= ", ";
		}
		$glob .= $var->{'name'};
		$glob .= ", ";
	    }
	}
	if (defined($req->{'valueparam'})) {
	    foreach my $var (@{$req->{'valueparam'}}) {
		$glob .= $var->{'value-mask-name'};
		$glob .= ", ";
		$glob .= $var->{'value-list-name'};
		$glob .= ", ";
	    }
	}
	chop $glob; chop $glob;  # removing trailing comma
	print OUT "$glob);\n\n";
	
	# Cleanup
	if (defined($req->{'list'})) {
	    foreach my $var (@{$req->{'list'}}) {
		if ($var->{'type'} ne 'char' and $var->{'type'} ne 'void') {
		    print OUT "    free(". $var->{'name'}.");\n";
		}
	    }
	}
	
	if (defined($req->{'valueparam'})) {
	    foreach my $var (@{$req->{'valueparam'}}) {
		print OUT "    free(". $var->{'value-list-name'}.");\n";
	    }
	}

	my $retcount = 0;
	$retcount = 1 if (defined($req->{'reply'}));
	print OUT "    return $retcount;\n}\n\n";

	my $manglefunc = mangle($req->{'name'}, 1);
	$func->{$manglefunc} = $req->{'name'};
    }
}

sub do_events($)
{
    my $xcb = shift;
    my %events;
    
    foreach my $event (@{$xcb->{'event'}}) {
	my $xcbev = mangle($event->{'name'})."_event_t";
	print OUT "/* This function adds the remaining fields into the table\n  that is on the top of the stack */\n";
	print OUT "static void set_";
	print OUT $event->{'name'};
	print OUT "(lua_State *L, xcb_generic_event_t *event)\n{\n";
 	print OUT "    $xcbev *x = ($xcbev *)event;\n";
	foreach my $var (@{$event->{'field'}}) {
	    my $name = $var->{'name'};
	    do_push(1, $var->{'type'}, $name);
	    print OUT "    lua_setfield(L, -2, \"$name\");\n";
	}
	print OUT "}\n\n";
	$events{$event->{'number'}} = 'set_'.$event->{'name'};
    }
    
    foreach my $event (@{$xcb->{'eventcopy'}}) {
	$events{$event->{'number'}} = 'set_'.$event->{'ref'};
    }
    
    print OUT "static void init_events()\n{\n";
    foreach my $i (sort { $a <=> $b } keys %events) {
	print OUT "    RegisterEvent($i, $events{$i});\n";
    }
    print OUT "}\n\n";
}

sub do_replies($\%\%)
{
    my $xcb = shift;
    my $func = shift;
    my $collect = shift;
    
    foreach my $req (@{$xcb->{'request'}}) {
	my $rep = $req->{'reply'};
	next unless defined($rep);
	
	my $name = mangle($req->{'name'});
	my $cookie = $name."_cookie_t";
	
	$collect->{$cookie} = $req->{'name'}."_gc";

	# Garbage collection function
	print OUT "static int ";
	print OUT $req->{'name'}.'_gc';
	print OUT "(lua_State *L)\n{\n";
	print OUT "    $cookie *cookie = ($cookie *)luaL_checkudata(L, 1, \"$cookie\");\n";
	print OUT "    lua_getfenv(L, 1);\n";
	print OUT "    lua_getfield(L, -1, \"collect\");\n";
	print OUT "    if (lua_toboolean(L, -1)) {\n";
	print OUT "        xcb_connection_t *c;\n";
	print OUT "        xcb_generic_error_t *e = NULL;\n";
	print OUT "        $name"."_reply_t *x = NULL;\n";
	print OUT "        lua_getfield(L, -2, \"display\");\n";
	print OUT "        c = ((xcb_connection_t **)luaL_checkudata(L, -1, \"XCB.display\"))[0];\n\n";
	
	print OUT "        lua_getfenv(L, -1);\n";
	print OUT "        lua_getfield(L, -1, \"closed\");\n";
	print OUT "        if (!lua_toboolean(L, -1))\n";
	print OUT "            x = $name"."_reply(c, *cookie, &e);\n\n";
	
	print OUT "        if (x) free(x);\n";
	        # TODO: Needs a way to report errors in GC'd replies
	print OUT "        if (e) free(e);\n";
	print OUT "    }\n";
	print OUT "    return 0;\n";
	print OUT "}\n\n";
	
	# Function header
	print OUT "static int ";
	print OUT $req->{'name'}.'_reply';
	print OUT "(lua_State *L)\n{\n";
	
	# Declare variables
	print OUT "    xcb_connection_t *c;\n";
	print OUT "    xcb_generic_error_t *e = NULL;\n";
	print OUT "    $name"."_reply_t *x;\n";
	print OUT "    $cookie *cookie;\n";
	print OUT "\n";
	
	# Read variables from lua
	print OUT "    c = ((xcb_connection_t **)luaL_checkudata(L, 1, \"XCB.display\"))[0];\n";
	print OUT "    lua_getfenv(L, 1);\n";
	print OUT "    lua_getfield(L, -1, \"closed\");\n";
	print OUT "    if (lua_toboolean(L, -1))\n";
	print OUT "        luaL_error(L, \"Error: already disconnected\");\n";
	print OUT "    lua_pop(L, 2);\n\n";
	
	print OUT "    cookie = ($cookie *)luaL_checkudata(L, 2, \"$cookie\");\n";
	print OUT "\n";
	
	# Function call
	print OUT "    x = $name"."_reply(c, *cookie, &e);\n\n";
	
	# Mark reply as not collectable
	print OUT "    lua_getfenv(L, 2);\n";
	print OUT "    lua_getfield(L, -1, \"collect\");\n";
	print OUT "    if (!lua_toboolean(L, -1)) {\n";
	print OUT "        luaL_error(L, \"Error: Attempted to wait for the same %s cookie twice\", \"$name\");\n";
	print OUT "    }\n";
	print OUT "    lua_pushboolean(L, 0);\n";
	print OUT "    lua_setfield(L, -3, \"collect\");\n";
	print OUT "    lua_pop(L, 2);\n\n";
	
	# Push reply to lua
	print OUT "    if (x) {\n";
	print OUT "        lua_newtable(L);\n";
	foreach my $var (@{$rep->[0]->{'field'}}) {
	    my $name = $var->{'name'};
	    do_push(2, $var->{'type'}, $name);
	    print OUT "        lua_setfield(L, -2, \"$name\");\n";
	}
	if (defined $rep->[0]->{'list'}) {
	    foreach my $list (@{$rep->[0]->{'list'}}) {
		do_push_list(2, $req->{'name'}, $list->{'type'}, $list->{'name'}, $list->{'value'});
	    }
	}
	
	print OUT "        free(x);\n";
	print OUT "    } else\n";
	print OUT "        lua_pushnil(L);\n\n";
	
	print OUT "    if (e) {\n";
	print OUT "        push_ERROR(L, e);\n";
	print OUT "        free(e);\n";
	print OUT "    } else\n";
	print OUT "        lua_pushnil(L);\n\n";
	
	print OUT "    return 2;\n}\n\n";
	
	my $manglefunc = mangle($req->{'name'}, 1);
	$func->{$manglefunc.'_reply'} = $req->{'name'}.'_reply';
    }
}

sub do_enums($)
{
    my $xcb = shift;
    
    print OUT "/* This function adds enums into the table that is on the top of the stack */\n";
    print OUT "static void set_enums(lua_State *L)\n{\n";
    
    foreach my $enum (@{$xcb->{'enum'}}) {
	print OUT "    lua_newtable(L);\n";
	foreach my $item (@{$enum->{'item'}}) {
	    my $value = $item->{'value'};
	    my $bit = $item->{'bit'};
	    if ($value) {
		print OUT "    lua_pushinteger(L, $value->[0]);\n";
	    } elsif ($bit) {
		print OUT "    lua_pushinteger(L, 1u << $bit->[0]);\n";
	    } else {
		die ("Unexpected enum type on ".$item->{'name'}."\n");
	    }
	    
	    my $name = mangle($item->{'name'}, 1);
	    $name = uc($name);
	    print OUT "    lua_setfield(L, -2, \"$name\");\n";
	}
	my $name = mangle($enum->{'name'}, 1);
	$name = uc($name);
	print OUT "    lua_setfield(L, -2, \"$name\");\n";
    }

    # Events
    print OUT "\n    lua_newtable(L);\n";
    foreach my $event (@{$xcb->{'event'}}) {
	my $name = $event->{'name'};
	my $number = $event->{'number'};
	print OUT "    lua_pushinteger(L, $number);\n";
	print OUT "    lua_setfield(L, -2, \"$name\");\n";
    }
    
    foreach my $event (@{$xcb->{'eventcopy'}}) {
	my $name = $event->{'name'};
	my $number = $event->{'number'};
	print OUT "    lua_pushinteger(L, $number);\n";
	print OUT "    lua_setfield(L, -2, \"$name\");\n";
    }
    print OUT "    lua_setfield(L, -2, \"event\");\n";
    
    # Errors
    print OUT "\n    lua_newtable(L);\n";
    foreach my $error (@{$xcb->{'error'}}) {
	my $name = $error->{'name'};
	my $number = $error->{'number'};
	print OUT "    lua_pushinteger(L, $number);\n";
	print OUT "    lua_setfield(L, -2, \"$name\");\n";
    }
    
    foreach my $error (@{$xcb->{'errorcopy'}}) {
	my $name = $error->{'name'};
	my $number = $error->{'number'};
	print OUT "    lua_pushinteger(L, $number);\n";
	print OUT "    lua_setfield(L, -2, \"$name\");\n";
    }
    print OUT "    lua_setfield(L, -2, \"error\");\n";
    
    print OUT "}\n\n";
}

sub do_gcs($\%)
{
    my $xcb = shift;
    my $collect = shift;
    
    print OUT "static void init_gcs(lua_State *L)\n{\n";
    foreach my $cookie (keys(%$collect)) {
	my $func = $collect->{$cookie};
	print OUT "    luaL_newmetatable(L, \"$cookie\");\n";
	print OUT "    lua_pushcfunction(L, $func);\n";
	print OUT "    lua_setfield(L, -2, \"__gc\");\n";
	print OUT "    lua_pop(L, 1);\n";
    }
    print OUT "}\n\n";
}

sub do_init($\%)
{
    my $xcb = shift;
    my $func = shift;
    
    print OUT "/* This function adds function calls into the table\n   that is on the top of the stack */\n";
    print OUT "void init_".$xcb->{'header'}."(lua_State *L)\n{\n";
    print OUT "    init_events(L);\n";
    print OUT "    init_gcs(L);\n";
    print OUT "    set_enums(L);\n";
    foreach my $name (keys %{$func}) {
	my $funcname = $func->{$name};
	print OUT "    lua_pushcfunction(L, $funcname);\n";
	print OUT "    lua_setfield(L, -2, \"$name\");\n";
    }
    print OUT "}\n";
}


my @files;

opendir(DIR, '.');
@files = grep { /\.xml$/ } readdir(DIR);
closedir DIR;

foreach my $name (@files) {
    $name =~ s/\.xml$//;

    my $xcb = XMLin("$name.xml", KeyAttr => undef, ForceArray => 1);

    open(OUT, ">$name.c") or die ("Cannot open $name.c for writing");

print OUT <<eot
/*
 * This file generated automatically from $name.xml by lua-binding.pl
 * Edit at your peril.
 */

#include <string.h>
#include <stdlib.h>

#include <lua5.1/lua.h>
#include <lua5.1/lauxlib.h>

#include <xcb/xcb.h>
#include <xcb/xcbext.h>
#include <xcb/$name.h>

typedef void (*eventFunc)(lua_State *L, xcb_generic_event_t *);
extern void RegisterEvent(int index, eventFunc func);

void push_ERROR(lua_State *L, xcb_generic_error_t *e)
{
    xcb_value_error_t *ve = (xcb_value_error_t *)e;
    lua_newtable(L);

    lua_pushinteger(L, ve->response_type);
    lua_setfield(L, -2, "response_type");
    lua_pushinteger(L, ve->error_code);
    lua_setfield(L, -2, "error_code");
    lua_pushinteger(L, ve->sequence);
    lua_setfield(L, -2, "sequence");
    lua_pushinteger(L, ve->bad_value);
    lua_setfield(L, -2, "bad_value");
    lua_pushinteger(L, ve->minor_opcode);
    lua_setfield(L, -2, "minor_opcode");
    lua_pushinteger(L, ve->major_opcode);
    lua_setfield(L, -2, "major_opcode");

    lua_pushinteger(L, e->full_sequence);
    lua_setfield(L, -2, "full_sequence");
}

eot
    ;
    
    my %functions;
    my %collectors;
    do_typedefs($xcb);
    do_structs($xcb);
    do_events($xcb);
    do_requests($xcb, %functions);
    do_replies($xcb, %functions, %collectors);
    do_enums($xcb);
    do_gcs($xcb, %collectors);
    do_init($xcb, %functions);
    close OUT;
}
