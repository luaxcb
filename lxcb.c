#include <stdio.h>
#include <stdlib.h>
#include <lua5.1/lua.h>
#include <lua5.1/lauxlib.h>

#include <xcb/xcb.h>

#define MT_DISPLAY "XCB.display"


extern void push_ERROR(lua_State *L, xcb_generic_error_t *e);

typedef void (*eventFunc)(lua_State *L, xcb_generic_event_t *);
eventFunc push_event[256] = { NULL };

void RegisterEvent(int index, eventFunc func)
{
    push_event[index] = func;
}

static void PushEvent(lua_State *L, xcb_generic_event_t *event)
{
    lua_newtable(L);
    lua_pushinteger(L, event->response_type);
    lua_setfield(L, -2, "response_type");
    lua_pushinteger(L, event->sequence);
    lua_setfield(L, -2, "sequence");
    lua_pushinteger(L, event->full_sequence);
    lua_setfield(L, -2, "full_sequence");
    
    if (push_event[event->response_type])
	push_event[event->response_type](L, event);
}

static int Connect(lua_State *L)
{
    xcb_connection_t *c;
    int screen;
    const char *name = NULL;
    
    if (!lua_isnil(L, 1))
	name = lua_tostring(L, 1);
    c = xcb_connect(name, &screen);
    if (!c)
        return 0;
    
    lua_newuserdata(L, sizeof(xcb_connection_t *));
    ((xcb_connection_t **)lua_touserdata(L, -1))[0] = c;
    luaL_getmetatable(L, MT_DISPLAY);
    lua_setmetatable(L, -2);
    
    lua_newtable(L);
    lua_setfenv(L, -2);
    
    lua_pushinteger(L, screen);
    return 2;
}

static int gc_connection(lua_State *L)
{
    xcb_connection_t *c;
    c = ((xcb_connection_t **)luaL_checkudata(L, 1, MT_DISPLAY))[0];
    
    lua_getfenv(L, 1);
    lua_getfield(L, -1, "closed");
    if (!lua_toboolean(L, -1))
	xcb_disconnect(c);

    return 0;
}

static int Disconnect(lua_State *L)
{
    xcb_connection_t *c;
    c = ((xcb_connection_t **)luaL_checkudata(L, 1, MT_DISPLAY))[0];
    
    lua_getfenv(L, 1);
    lua_getfield(L, -1, "closed");
    if (lua_toboolean(L, -1))
	luaL_error(L, "Error: already disconnected");

    lua_pushboolean(L, 1);
    lua_setfield(L, -3, "closed");
    
    xcb_disconnect(c);
    return 0;
}

static int Flush(lua_State *L)
{
    xcb_connection_t *c;
    c = ((xcb_connection_t **)luaL_checkudata(L, 1, MT_DISPLAY))[0];
    
    lua_pushinteger(L, xcb_flush(c));
    return 1;
}

static int GenerateID(lua_State *L)
{
    lua_pushinteger(L, xcb_generate_id(((xcb_connection_t **)luaL_checkudata(L, 1, MT_DISPLAY))[0]));

    return 1;
}


extern void push_Setup(lua_State *L, const xcb_setup_t *x); //Temporary until I get headers sorted out

static int GetSetup(lua_State *L)
{
    xcb_connection_t *c;
    const xcb_setup_t *setup;
    
    c = ((xcb_connection_t **)luaL_checkudata(L, 1, MT_DISPLAY))[0];
    
    lua_getfenv(L, 1);
    lua_getfield(L, -1, "closed");
    if (lua_toboolean(L, -1))
	luaL_error(L, "Error: already disconnected");

    setup = xcb_get_setup(c);
    if (!setup)
	luaL_error(L, "Error: connection failed");
    
    push_Setup(L, setup);
    return 1;
}

static int PollForEvent(lua_State *L)
{
    xcb_connection_t *c;
    xcb_generic_event_t *e;
    
    c = ((xcb_connection_t **)luaL_checkudata(L, 1, MT_DISPLAY))[0];

    lua_getfenv(L, 1);
    lua_getfield(L, -1, "closed");
    if (lua_toboolean(L, -1))
	luaL_error(L, "Error: already disconnected");

    e = xcb_poll_for_event(c);
    if (!e)
	lua_pushnil(L);
    else if (!e->response_type)
	push_ERROR(L, (xcb_generic_error_t *)e);
    else
	PushEvent(L, e);
    
    if (e)
	free(e);
    
    return 1;
}

static int WaitForEvent(lua_State *L)
{
    xcb_connection_t *c;
    xcb_generic_event_t *e;
    
    c = ((xcb_connection_t **)luaL_checkudata(L, 1, MT_DISPLAY))[0];

    lua_getfenv(L, 1);
    lua_getfield(L, -1, "closed");
    if (lua_toboolean(L, -1))
	luaL_error(L, "Error: already disconnected");

    e = xcb_wait_for_event(c);
    if (!e)
	lua_pushnil(L);
    else if (!e->response_type)
	push_ERROR(L, (xcb_generic_error_t *)e);
    else
	PushEvent(L, e);
    
    if (e)
	free(e);
    
    return 1;
}

static int HasError(lua_State *L)
{
    xcb_connection_t *c;
    c = ((xcb_connection_t **)luaL_checkudata(L, 1, MT_DISPLAY))[0];
    
    lua_getfenv(L, 1);
    lua_getfield(L, -1, "closed");
    if (lua_toboolean(L, -1))
	luaL_error(L, "Error: already disconnected");

    lua_pushboolean(L, xcb_connection_has_error(c));
    return 1;
}



static luaL_Reg lxcb[] = {
    { "connect", Connect },
    { NULL, NULL }
};

static luaL_Reg display_m[] = {
    { "__gc", gc_connection },
    { "disconnect", Disconnect },
    { "flush", Flush },
    { "generate_id", GenerateID },
    { "get_setup", GetSetup },
    { "poll_for_event", PollForEvent },
    { "wait_for_event", WaitForEvent },
    { "has_error", HasError },
    /* TODO:
    { "connect_to_display_with_auth_info", ConnectToDisplayWithAuthInfo },
    { "connect_to_fd", ConnectToFD },
    { "get_file_descriptor", GetFileDescriptor },
    { "get_maximum_request_length", GetMaximumRequestLength },
    { "parse_display", ParseDisplay },
    { "prefetch_extension_data", PrefetchExtensionData },
    { "prefetch_maximum_request_length", PrefetchMaximumRequestLength },
    { "query_extension_reply", QueryExtensionReply },
    { "request_check", RequestCheck },
    */   
    { NULL, NULL }
};

#ifdef _MSC_VER
#define DLLEXPORT __declspec(dllexport)
#else
/* Must be gcc if not MSC */
#define DLLEXPORT __attribute__((visibility("default")))
#endif

extern void init_xproto(lua_State *L); // Temporary, until I get headers sorted.

DLLEXPORT int luaopen_lxcb(lua_State *L)
{
    luaL_register(L, "lxcb", lxcb);

    luaL_newmetatable(L, MT_DISPLAY);
    lua_pushvalue(L, -1);
    lua_setfield(L, -2, "__index");
    luaL_register(L, NULL, display_m);

    init_xproto(L);
    lua_pop(L, 1);

    return 1;
}
